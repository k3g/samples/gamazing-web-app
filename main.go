package main

import (
	"fmt"
	"net/http"
	"os"
)

var greating string = `<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hello World!</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
        .title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
        .subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
        .sign { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 22px; color: #ad9b77; word-spacing: 5px; padding-bottom: 15px; }
        .links { padding-top: 15px; }
    </style>
  </head>
  <body>
    <section class="container">
      <div>
        <h1 class="title">
          👋 Hello World 🌍
        </h1>
        <h2 class="subtitle">
          Made with 💚 and Golang
        </h2>
        <h3 class="sign">
          By ???
        </h3>
      </div>
    </section>
  
  
</body></html>`

func hello(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, greating)
}

func main() {

	PORT, ok := os.LookupEnv("PORT")
	if ok && len(PORT) > 0 {
		PORT = ":" + PORT
	} else {
		PORT = ":8080"
	}

	http.HandleFunc("/", hello)

	fmt.Printf("Listening on port %s", PORT)

	http.ListenAndServe(PORT, nil)
}
