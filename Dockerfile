FROM golang:alpine AS build-env
WORKDIR /build
ADD main.go /build
ENV CGO_ENABLED=0
ENV GOOS=linux
RUN cd /build && go build -ldflags="-w -s" -o hello

FROM scratch
WORKDIR /app
COPY --from=build-env /build/hello /app/hello
ENTRYPOINT ["/app/hello"]